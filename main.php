<!DOCTYPE HTML>
<!--
	TXT by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Jarrettsville Tae Kwon Do</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="logo container">
					<div>
						
						<h1><image src = "images/WebLogo.jpg"> </image></h1><br />
						<p>Courtesy, Perseverance, Self-Control, Loyalty, Integrity, Patience, Respect, and Confidence. </p>
					</div>
				</div>
			</header>

		<!-- Nav -->
			<nav id="nav" class="skel-layers-fixed">
				<ul>
					<li class="current"><a href="main.html">Home</a></li>
					<li>
						<a href="NewStudentsMain.html">New Students</a>
						<ul>
							<li><a href="gettingStarted.html">Getting Started</a></li>
							<li><a href="NewStudentSchoolDetails.html">School Details</a></li>
							<li><a href="FAQ.html">Frequently Asked Questions</a></li>
							<li> <a href="https://drive.google.com/open?id=0ByXSze3f7KSYLXdrQ25aMXlpNkU" target="_blank">Registration and Waiver Forms</a></li>
						</ul>
					</li>
					<li>
					
						<a href="CurrentStudentsMain.html">Current Students</a>
						<ul>
							<li><a href="#">What You Should Know</a>
								<ul>
									<li><a href="#">School Rules</a></li>
									<li><a href="#">Terminology</a></li>
									<li><a href="#">How To Tie A Belt</a></li>
								</ul>
							</li>
							</li>
							<li><a href="">Techniques</a>
								<ul>
									<li><a href="#">Poomse</a>
										<ul>
											<li><a href="#">Basic Form</a></li>
											<li><a href="#">Taeguk One</a></li>
											<li><a href="#">Taeguk Two</a></li>
											<li><a href="#">Taeguk Three</a></li>
											<li><a href="#">Taeguk Four</a></li>
											<li><a href="#">Taeguk Five</a></li>
											<li><a href="#">Taeguk Six</a></li>
											<li><a href="#">Taeguk Seven</a></li>
											<li><a href="#">Taeguk Eight</a></li>
											<li><a href="#">Koryo</a></li>
										</ul>
									</li>
									<li><a href="https://drive.google.com/open?id=0ByXSze3f7KSYbzJoVmZZTDM4d1pTQ0pOZWFQWUFUeXBnLVA0" target="_blank">Self Defense</a></li>
									<li><a href="https://drive.google.com/open?id=0ByXSze3f7KSYV3MxdGM4N2ladUhRdTk4ZTI4S05jWmRrTjFv" target="_blank">One Steps</a></li>
								</ul>
							</li>
							<li><a href="#">Calendar</a>
							<li><a href="#">Training Tips</a></li>							
							<li><a href="#">Master Joe's Articles</a></li>
						</ul>
					</li>
					<li><a>Benefits of TKD</a></li>
					<li><a>Photos</a></li>
					<li><a>Instructors</a>
						<ul>
							<li><a>Master Ken Chamberlain</a></li>
							<li><a>Remembering Master Joe Nawrozki</a></li>
							<li><a>Other Instructors</a></li>
						</ul>
					</li>
					<li><a>Contact Us</a></li>
				</ul>
			</nav>
		
		<!-- Main -->
			<div id="main-wrapper">
				<div id="main" class="container">
					<div class="row">
						<div class="3u">
							<div class="sidebar">
							
								<!-- Sidebar -->
							
									<!-- Recent Posts -->
										<section>
											<h2 class="major"><span>Announcements</span></h2>
											<ul class="divided">
												<li>
													<article class="box post-summary">
														<h3>Monday's & Wednesday's @ North Bend Elementary School from 7:30-9:30pm</h3>
														<!--<ul class="meta">
															<li class="icon fa-clock-o">6 hours ago</li>
															<li class="icon fa-comments"><a href="#">34</a></li>
														</ul> -->
													</article>
												</li>
												<li>
													<article class="box post-summary">
														<h3>Dates we will not have class:
															<br />9/11/15
															<br />9/11/15
														</h3>
													</article>
												</li>
												<li>
													<article class="box post-summary">
														<h3>The website is currently being updated. If you have any concerns, contact Sean. sean.r.graff@gmail.com</h3>
														<ul class="meta">
															<li class="icon fa-clock-o">8/7/2015</li>
														</ul>
													</article>
												</li>
												
											</ul>
											<!-- <a href="#" class="button alt">Browse Archives</a> -->
										</section>

									<!-- Something 
										<section>
											<h2 class="major"><span>Ipsum Dolore</span></h2>
											<a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
											<p>
												Donec sagittis massa et leo semper scele risque metus faucibus. Morbi congue mattis mi. 
												Phasellus sed nisl vitae risus tristique volutpat. Cras rutrum sed commodo luctus blandit.
											</p>
											<a href="#" class="button alt">Learn more</a>
										</section> -->

									<!-- Something 
										<section>
											<h2 class="major"><span>Magna Feugiat</span></h2>
											<p>
												Rhoncus dui quis euismod. Maecenas lorem tellus, congue et condimentum ac, ullamcorper non sapien. 
												Donec sagittis massa et leo semper scele risque metus faucibus. Morbi congue mattis mi. 
												Phasellus sed nisl vitae risus tristique volutpat. Cras rutrum sed commodo luctus blandit.
											</p>
											<a href="#" class="button alt">Learn more</a>
										</section>-->

							</div>
						</div>
						<div class="9u important(collapse)">
							<div class="content content-right">
							
								<!-- Content -->
						
									
										<section class="box features">
											<h2 class="major"><span>Welcome To The Jarrettsville TKD Website!</span></h2>
											<div>
												
												<div class="row">
												<p>Jarrettsville Tae Kwon Do is a Jarrettsville Parks and Recreation Program, a non-profit organization.</p>
												
												<p>Our Tae Kwon Do program is great for people of all ages. We are dedicated to providing high quality Tae Kwon Do 
												training at an affordable price. Our cheif instructor, Master Ken Chamberlain, is a Fifth-Dan Black Belt registered 
												with the World Tae kwon do Federation in Seoul, Korea, and he has studied, competed, coached and taught Tae Kwon Do 
												for a major portion of his life.</p>
												
												<p>We encourage Tae Kwon Do as a family activity. Jarrettsville Tae Kwon Do is much more than a program that teaches 
												self-defense. Our program provides a chance for youth and adults to develop qualities that will benefit them for the 
												rest of their lives. These qualities include: <strong>Courtesy, Perseverance, Self-Control, Loyalty, Integrity, Patience, 
												Respect, and Confidence</strong>. Our art and sport is grounded in the the Korean traditions; mind, body and spirit are 
												fused to shape the complete martial artist. Our goal is for our students to develop an indomitable spirit that leads them to 
												victory in the challenges they face throughout life. Every effort is made to connect the training in martial arts to real life skills. </p>
												
												<p>Proper hydration and self-care is stressed, and athletes are not encouraged to "walk off" an injury, no matter how minor. 
												Our class includes three sections - Tae Kwon Do (fighting and form) , Hap Ki Do (self- defense), and Judo (throwing, sweeping 
												falling and engaging in mat combat). In addition, guest speakers are invited to class to discuss issues ranging from sports training 
												to life skills. Beyond becoming a skilled martial artist, the primary goal of Jarrettsville Tae Kwon Do is to broaden and improve the 
												character of the students. Jarrettsville Tae Kwon Do is the only non-profit school that competes in the State and National Junior Olympics. 
												We are very proud of our Jarrettsville Tae Kwon Do Tournament Team who has captured numerous gold, silver and bronze medals in forms and 
												sparring competitions at both the State and National level.</p>
											</section>

							</div>
						</div>
					</div>
					<div class="row 200%">
						<div class="12u">

							<!-- Features -->
								<section class="box features">
									<h2 class="major"><span><a href = "FAQ.html">Frequently Asked Questions</a></span></h2>
									<div>
										<div class="row">
											<div class="3u">
												
												<!-- Feature -->
													<section class="box feature">
														<a href="FAQ.html" class="image featured"><img src="images/IMG_8802.JPG" alt="" /></a>
														<h3><a href="FAQ.html">Do I need any previous experience?</a></h3>
														<p>
															No. Your first class can be intimidating...all those white uniforms, all those different colored belts, 
															the Korean words! Don't worry, everyone in class had a first day and remembers what it is like. The more 
															experienced students will help you, and the instructors will be watching to help you out. Just keep your 
															ears and eyes open and follow the lead of the experienced students.
														</p>
													</section>
										
											</div>
											<div class="3u">
												
												<!-- Feature -->
													<section class="box feature">
														<a href="FAQ.html" class="image featured"><img src="images/IMG_8920.JPG" alt="" /></a>
														<h3><a href="FAQ.html">How do I start?</a></h3>
														<p>
															Start by filling out the <a href="https://drive.google.com/open?id=0ByXSze3f7KSYLXdrQ25aMXlpNkU" target="_blank">Registration and Waiver forms</a>. We have two primary 
															registration days each year, one in September and one in January. 
															However, this is an open enrollment program, and we welcome students year round. 
															Come to the school and you will be welcomed with open arms!
														</p>
													</section>
										
											</div>
											
											<div class="3u">
												
												<!-- Feature -->
													<section class="box feature">
														<a href="FAQ.html" class="image featured"><img src="images/IMG_8872.JPG" alt="" /></a>
														<h3><a href="FAQ.html">Should I be concerned about sparring?</a></h3>
														<p>
															Fighting may be an initial concern for a new student, but there is no reason to worry. 
															Sparring in class is closely supervised by instructors with years of experience. In 
															addition all sparring is done with protective equipment (head gear, arm pads, shin 
															and foot pads, chest protectors). All sparring is performed with controlled contact. Although 
															injuries are part of any sport, injuries are infrequent and usually minor in nature. For most 
															students, their first sparring experience is with an instructor or very experienced upper belt 
															to help them learn and prevent any injuries.
														</p>
													</section>
										
											</div>
											
											<div class="3u">
												
												<!-- Feature -->
													<section class="box feature">
														<a href="FAQ.html" class="image featured"><img src="images/IMG_8857.JPG" alt="" /></a>
														<h3><a href="FAQ.html">Why Jarrettsville TKD?</a></h3>
														<p>
															We offer high quality training at an affordable price.You will be taught 
															by experienced instructors and have support from our entire class. Master 
															Ken is an experienced martial artist and teacher, well acquainted with sports 
															training and sports psychology. Our belief at JTKD is that while students should 
															be encouraged to try hard, they should not be beaten down emotionally. Our 
															dedicated instructors treat all students with respect and patience as 
															students progress in their TKD journey. Our instructors insist on proper 
															form to prevent injuries, and students and instructors receive training in first aide. 
															Every effort is made to give each student a safe and healthy start in the martial arts.
														</p>
													</section>
										
											</div>
										</div>
										
										<center>
										<div class="row">
											<div class="3u">
												
												<!-- Feature -->
													<section class="box feature">
														<a href="FAQ.html" class="image featured"><img src="images/IMG_8866.JPG" alt="" /></a>
														<h3><a href="FAQ.html">Are belts from other schools recognized?</a></h3>
														<p>
															Sometimes yes and sometimes no. When you come to class Master Ken will assess your skills and determine your rank in this school.
														</p>
													</section>
										
											</div>
											<div class="3u">
												
												<!-- Feature -->
													<section class="box feature">
														<a href="FAQ.html" class="image featured"><img src="images/IMG_8830.JPG" alt="" /></a>
														<h3><a href="FAQ.html">When and where does Jarrettsville TKD meet?</a></h3>
														<p>
															Jarrettsville Tae Kwon Do classes are held at North Bend Elementary School in Jarrettsville, Maryland, on Monday and Wednesday night from 7:30 to 9:00.
														</p>
													</section>
										
											</div>
											<div class="3u">
												
												<!-- Feature -->
													<section class="box feature">
														<a href="FAQ.html" class="image featured"><img src="images/IMG_8827.JPG" alt="" /></a>
														<h3><a href="FAQ.html">Will there be students my age?</a></h3>
														<p>
															Jarrettsville TKD is composed of male and female students as young as 7 years old to students 50+ years old.
														</p>
													</section>
										
											</div>
											
										</div>
									</div>
									</center>
								</section>

						</div>
					</div>
				</div>
			</div>

		<!-- Footer -->
			<footer id="footer" class="container">
				<div class="row 200%">
					<div class="12u">

						<!-- Contact -->
							<section>
								<h2 class="major"><span>Get in touch</span></h2>
								<ul class="contact">
									<li><a class="icon fa-facebook" href="https://www.facebook.com/pages/Jarrettsville-Tae-Kwon-Do/501783036552124"><span class="label">Facebook</span></a></li>
									<li><a class="icon fa-google-plus" href="#"><span class="label">Google+</span></a></li>
								</ul>
							</section>
					
					</div>
				</div>

				<!-- Copyright -->
					<div id="copyright">
						<ul class="menu">
							<li>&copy; JarrettsvilleTKD. All rights reserved</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</div>

			</footer>

	</body>
</html>